# Newsletter

Sert de couche d'abstraction entre les différentes plateformes de newsletter.

```php
interface NewsletterInterface
{
    /*
    Permet d'injecter un array de configuration ou rien si on veut utiliser le fichier 
    de configuration de base fourni avec le package. Permet d'utiliser les fonctions 
    de laravel, symfony, etc. pour loader les configurations.
    */
    public function config(?array $config = null): void;
}

// Exemple d'injection de configuration
class NewsletterController
{
    public function subscribe($request)
    {
        //Laravel config loader
        /* newsletter.php => ['mailchimp'=>['key'=>...,'secret'=>...]] */
        $mailchimp = config('newsletter');
        $adapter = new NewsletterInterfaceAdapter();
        $adapter->config($mailchimp);
        $adapter->subscribe($list, ['email'=>$request->get('email')]);
    }
}
```

# Adapter disponibles
* [Klaviyo](https://bitbucket.org/quatrecentquatre/cam16001-refonte-site-web-camellia/src/1f2b7ba7588771a6ced1d5e6064291e9f5698695/production/htdocs/packages/qcq/newsletter/src/Adapters/KlaviyoAdapter.php?at=development&fileviewer=file-view-default)

# Interfaces disponibles

Interfaces existantes
* [NewsletterInterface](#newsletterinterface)
* [ListNewsletterInterface](#listnewsletterinterface)


## NewsletterInterface <a href="#newsletterinterface"></a>

```php
interface NewsletterInterface
{
    public function config(?array $config = null): void;
}
```

## ListNewsletterInterface <a href="#listnewsletterinterface"></a>

```php
interface ListNewsletterInterface extends NewsletterInterface
{
    public function subscribe(string $list, array $payload): array;
    public function unsubscribe(string $list, array $payload): array;
}
```