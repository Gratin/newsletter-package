<?php
namespace Gratin\Newsletter\Interfaces;

use Gratin\Newsletter\Interfaces\NewsletterInterface;

interface MetricsNewsletterInterface extends NewsletterInterface
{
    public function metric();
}
