<?php
namespace Gratin\Newsletter\Interfaces;

use Gratin\Newsletter\Interfaces\NewsletterInterface;

interface ListNewsletterInterface extends NewsletterInterface
{
    public function subscribe(string $list, array $payload): array;
    public function unsubscribe(string $list, array $payload): array;
}
