<?php
namespace Gratin\Newsletter\Interfaces;

interface NewsletterInterface
{
    public function config(?array $config = null): void;
}
