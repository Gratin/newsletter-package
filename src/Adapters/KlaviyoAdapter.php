<?php
namespace Gratin\Newsletter\Adapters;

use Gratin\Newsletter\Http\KlaviyoClient;
use Gratin\Newsletter\Interfaces\ListNewsletterInterface;

class KlaviyoAdapter implements ListNewsletterInterface
{
    /**
     * @var KlaviyoClient $client The Klaviyo Http Client
     */
    private $client;

    /**
     * @var string $public The public API key
     */
    private $public;
    
    /**
     * @var string $private The private API key
     */
    private $private;

    /**
     * @var string SUBSCRIBE_ACTION The string that describes the subscribe action
     */
    public const SUBSCRIBE_ACTION   = "subscribe";

    /**
     * @var string UNSUBSCRIBE_ACTION The string that describes the subscribe action
     */
    public const UNSUBSCRIBE_ACTION = "unsubscribe";

    /**
     * @var string CHECKINLIST_ACTION The string that describes the subscribe action
     */
    public const CHECKINLIST_ACTION = "checkInList";

    public function __construct()
    {
        $this->client = new KlaviyoClient();
        $this->config();
    }

    /**
     * Launches the request to the client to subscribe a user to a given list if the payload
     * is valid
     * @param string $list The list ID
     * @param array $payload The payload to send to Klaviyo. The API key is injected before the request
     * @return array The response's content json-decoded
     */
    public function subscribe(string $list, array $payload): array
    {
        if (!$this->validatePayload(static::SUBSCRIBE_ACTION, $payload)) {
            throw new \Exception('klaviyo.invalid.payload');
        }

        return $this->client->addPayload('api_key', $this->private)->subscribe($list, $payload)->getContent();
    }

    /**
     * Launches the request to the client to unsubscribe a user to a given list if the payload
     * is valid
     * @param string $list The list ID
     * @param array $payload The payload to send to Klaviyo. The API key is injected before the request
     * @return array The response's content json-decoded
     */
    public function unsubscribe(string $list, array $payload): array
    {
        if (!$this->validatePayload(static::UNSUBSCRIBE_ACTION, $payload)) {
            throw new \Exception('klaviyo.invalid.payload');
        }

        return $this->client->addPayload('api_key', $this->private)->unsubscribe($list, $payload)->getContent();
    }

    /**
     * Launches the request to the client to unsubscribe a user to a given list if the payload
     * is valid
     * @param string $list The list ID
     * @param array $payload The payload to send to Klaviyo. The API key is injected before the request
     * @return array The response's content json-decoded
     */
    public function updateMember(string $userId, array $payload): array
    {
        return $this->client->addPayload('api_key', $this->private)->updateMember($userId, $payload)->getContent();
    }

    /**
     * Subscribes a user to a list
     * @param string $list The list ID
     * @param array $payload The payload to send
     * @return KlaviyoClient
     */
    public function checkMember(string $list, array $payload)
    {
        if (!$this->validatePayload(static::CHECKINLIST_ACTION, $payload)) {
            throw new \Exception('klaviyo.invalid.payload');
        }

        $payload = ['api_key' => $this->private] + $payload;
        return $this->client->addPayload('api_key', $this->private)->checkInList($list, $payload)->getContent();
    }


    /**
     * Validates a payload for a given action
     * @param string $action The action to validate
     * @param array $payload The payload to validate
     * @return bool
     */
    protected function validatePayload(string $action, array $payload): bool
    {
        switch ($action) {
            case self::UNSUBSCRIBE_ACTION:
                if (!array_key_exists('batch', $payload)) {
                    return false;
                }
                $emails = json_decode($payload['batch'], true);
                if (!count($emails)) {
                    return false;
                }

                if (!array_key_exists('email', $emails[0])) {
                    return false;
                }

                break;
            case self::SUBSCRIBE_ACTION:
            case self::CHECKINLIST_ACTION:
                if (!array_key_exists('email', $payload)) {
                    return false;
                }
                break;
            default:
                break;
        }

        return true;
    }

    /**
     * Configures the class' api keys
     * @param null|array $config The configuration to use as a base array instead of default config
     *          Used to inject configuration from frameworks or other means
     * @return void
     */
    public function config(?array $config = null): void
    {
        if (!$config) {
            $config = include(__DIR__.'/../config/newsletter.php');
        }
        try {
            $this->private  = $config['klaviyo']['private_key'];
            $this->public   = $config['klaviyo']['public_key'];
        } catch (\Exception $e) {
            throw new \Exception('Klaviyo configuration error. Make sure newsletter.php exists in your config folder and that klaviyo key has both private_key and public_key keys and values.');
        }
    }
}
