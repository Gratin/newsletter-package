<?php
return [
    'klaviyo'   =>  [
        'private_key'   =>  env('KLAVIYO_PRIVATE_KEY', ''),
        'public_key'    =>  env('KLAVIYO_PUBLIC_KEY', ''),
        'lists'         =>  [
            // list_name    => list_id
        ]
    ]
];
