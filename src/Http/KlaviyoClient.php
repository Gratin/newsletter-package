<?php
namespace Gratin\Newsletter\Http;

use GuzzleHttp\Client;

class KlaviyoClient
{
    /**
     * @var GuzzleHttp\Client $client An instance of Guzzle Http Client
     */
    private $client;

    /**
     * @var GuzzleHttp\Psr7\Response $response An instance of Guzzle Http Response
     */
    private $response;

    /**
     * @var string $listAdd The base Klaviyo api URL to add a member to a list
     */
    private $lists        =  [
        'subscribe'     =>  "https://a.klaviyo.com/api/v1/list/%s/members",
        'unsubscribe'   =>  "https://a.klaviyo.com/api/v1/list/%s/members/batch",
        'updateMember'  =>  "https://a.klaviyo.com/api/v1/person/%s",
    ];

    /**
     * @var array $headers The base Klaviyo api request headers
     */
    private $headers    = [
    ];

    /**
     * @var array $payload The base Klaviyo api request payload
     */
    private $payload    = [
    ];

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Sets the requests' authorization header to the given token
     * @param string $header The header value
     * @return KlaviyoClient
     */
    public function setAuthorizationHeader($header): KlaviyoClient
    {
        $this->headers['api_key'] = $header;

        return $this;
    }

    /**
     * Sets the requests' authorization header to the given token
     * @param string $key The Payload key
     * @param string $value The Payload value
     * @return KlaviyoClient
     */
    public function addPayload($key, $value): KlaviyoClient
    {
        $this->payload[$key] = $value;

        return $this;
    }

    /**
     * Retrieves the response's content
     * @return array json_decoded string of the response's body
     */
    public function getContent(): array
    {
        return @json_decode($this->response->getBody()->getContents(), true) ?? [];
    }

    /**
     * Subscribes a user to a list
     * @param string $list The list ID
     * @param array $payload The payload to send
     * @return KlaviyoClient
     */
    public function subscribe(string $list, array $payload): KlaviyoClient
    {
        try {
            $this->response = $this->client->post(
                $this->url($this->lists['subscribe'], $list),
                [
                    'form_params'   =>  $this->defaults($payload),
                    'headers'       =>  $this->headers
                ]
            );
        } catch (\Exception $e) {
            throw $e;
        }

        return $this;
    }

    /**
     * Check a user in list
     * @param string $list The list ID
     * @param array $payload The payload to send
     * @return KlaviyoClient
     */
    public function checkInList(string $list, array $payload)
    {
        try {
            $this->response = $this->client->get(
                $this->url($this->lists['subscribe'], $list),
                    [
                        'query'   => $this->defaults($payload),
                        'headers' => $this->headers
                    ]
                );
        } catch (\Exception $e) {
            throw $e;
        }

        return $this;
    }

    /**
     * Unsubscribes a user from a list
     * @param string $list The list ID
     * @param array $payload The payload to send
     * @return KlaviyoClient
     */
    public function updateMember(string $userId, array $payload)
    {
        try {
            $this->response = $this->client->put($this->url($this->lists['updateMember'], $userId), [
                'form_params'   =>  $this->defaults($payload),
                'headers'       =>  $this->headers
            ]);
        } catch (\Exception $e) {
            throw $e;
        }

        return $this;
    }

    /**
     * Update a user data
     * @param string $list The list ID
     * @param array $payload The payload to send
     * @return KlaviyoClient
     */
    public function unsubscribe(string $list, array $payload)
    {
        try {
            $this->response = $this->client->delete(
                $this->url($this->lists['unsubscribe'], $list),
                [
                    'form_params'   =>  $this->defaults($payload), 
                    'headers'       =>  $this->headers
                ]
            );
        } catch (\Exception $e) {
            throw $e;
        }

        return $this;
    }

    /**
     * Injects default post parameters (such as API_KEY)
     * @param array $payload The payload to merge with the base
     * @return array
     */
    private function defaults(array $payload): array
    {
        return ($this->payload + $payload);
    }

    /**
     * Builds a Klaviyo URL
     * @param string $key The key to append to the base url
     * @param string $arg The argument to build the url with
     * @return string
     */
    private function url($base, $arg)
    {
        return sprintf($base, $arg);
    }
}
